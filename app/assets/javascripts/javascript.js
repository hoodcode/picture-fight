$(function(){

    $("#add_image").on("click", function(e) {

        var request = $.ajax({
            url: "/images/new?w=1",
            type: "GET",
            dataType: "html"
        }).done(function(msg) {
                $( ".modal-body").html(msg);
                $('#add_image_modal').modal('toggle');
            }).fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
            });
        e.stopPropagation()
        e.preventDefault();
        return false;
    });


});