class VotesController < ApplicationController


  def create
    @vote = Vote.new
    @vote.user = current_user
    @vote.fight_id = params[:id]
    @vote.for_player_id = params[:for_player_id]
    @vote.save
    redirect_to(fight_path(params[:id]))
  end
end
