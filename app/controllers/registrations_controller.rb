class RegistrationsController < Devise::RegistrationsController
  before_filter :resource_name

  def index
  end

  def resource_name
    :user
  end

  def new
    @user=User.new
    render :'users/registrations/new'
  end

  def edit

    render :'users/registrations/edit'
  end

  def update

    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated

      sign_in @user, :bypass => true
      render :'users/registrations/update'

    else
      render "edit"
    end

  end

  def create
    super
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation, :current_password)
  end



end
