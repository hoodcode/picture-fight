class FightsController < ApplicationController


  def index
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @fights = Fight.active_fights.where(category_id: category.id).order(created_at: :desc)
    else
      @fights = Fight.active_fights.order(created_at: :desc)
    end

    @active_place = "latest"
    @categories = Category.all
  end

  def my
    @owner = User.where(username: params[:id]).first
    @active_place = params[:id]
    if signed_in?
      @active_place = "my" if params[:id]==current_user.username
    end
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @fights = Fight.where("player_1_id=? OR player_2_id=?", current_user.id, current_user.id).where(category_id: category.id).order(created_at: :desc )
    else
      @fights=Fight.where("player_1_id=? OR player_2_id=?", @owner.id, @owner.id).order(created_at: :desc)
    end
    @categories = assign_categories(@owner)
  end

  def show
     @fight = Fight.find(params[:id])
     @player_1 = User.find(@fight.player_1_id)
     @player_1_image = Image.find(@fight.player_1_image_id)


     if @fight.player_2_id.present?
        @player_2 = User.find(@fight.player_2_id)
        @player_2_image = Image.find(@fight.player_2_image_id)

     if @fight.datetime_end <= DateTime.now
       @closed = true
       if @player_1_votes_number > @player_2_votes_number
         @winner = "The winner is "+@player_1.username.to_s
       elsif @player_1_votes_number == @player_2_votes_number
         @winner = "It is a draw !"
       else
         @winner = "The winner is "+@player_2.username.to_s
       end
     end

     end

     @allow_to_vote = false
     if signed_in?
       @vote = Vote.new(fight_id: @fight.id, user_id: current_user.id)
       @votes = Vote.where(fight_id: @fight.id)
       @allow_to_vote = true if @vote.valid?
     end
     @player_1_votes_number = @fight.votes.where(for_player_id: @fight.player_1_id).count
     @player_2_votes_number = @fight.votes.where(for_player_id: @fight.player_2_id).count


     @active_place = params[:id]
  end

  def popular
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @fights = Fight.active_fights.where(category_id: category.id).order(votes_count: :desc)
    else
      @fights = Fight.active_fights.order(votes_count: :desc)
    end

    @active_place = "popular"
    @categories = Category.all
    render :index
  end

  def closed
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @fights = Fight.closed_fights.where(category_id: category.id).order(votes_count: :desc)
    else
      @fights = Fight.closed_fights.order(votes_count: :desc)
    end

    @active_place = "closed"
    @categories = Category.all
    render :index
  end

  def start_fight

    image = current_user.images.find(params[:image])

    open_fight = Fight.where(winner_id: nil).where("player_1_id!=?", current_user.id).where(player_2_id: nil).where(category_id: image.category_id)

    if open_fight.present?
      @fight = open_fight.first
      @fight.player_2 = current_user
      @fight.player_2_image = image
      @fight.datetime_end = DateTime.now + 1


    else
      @fight = Fight.new
      @fight.category = image.category
      @fight.player_1 = current_user
      @fight.player_1_image = image

    end
    if @fight.save
        redirect_to @fight
    else
      redirect_to images_path(current_user), :alert => "Error adding this picture to the fight"
    end
  end


  def assign_categories(owner)
    category_rows = Fight.where("player_1_id=? OR player_2_id=?", owner.id, owner.id).select("distinct category_id")
    category_ids = category_rows.collect{ |a| a[:category_id]}
    categories = Category.find(category_ids)
    return categories
  end


end
