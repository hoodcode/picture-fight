require "open-uri"
class ImagesController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create]



  def index
    @active_place = "currently fighting"
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @fights = Fight.active_fights.where(category_id: category.id)
    else
      @fights = Fight.active_fights
    end
      @images_player_1_ids = @fights.collect { |row| row[:player_1_image_id] }
      @images_player_2_ids = @fights.collect { |row| row[:player_2_image_id] }
      @images_ids = @images_player_1_ids + @images_player_2_ids
      @images = Image.find(@images_ids).reverse
      @categories = Category.all
  end

  def my
    @owner = User.where(username: params[:id]).first
    @active_place = params[:id]
    if signed_in?
      @active_place = "my" if params[:id]==current_user.username
    end
    if params[:category].present?
      category_name = params[:category]
      category = Category.where(name: category_name).first
      @images = @owner.images.where(category_id: category.id)
    else
      @images=@owner.images
    end
    assign_categories(@owner)
  end

  def new
    @active_place = "new"
    #redirect_to(new_image_path(current_user)) if params[:id]!=current_user.username
    @image=Image.new
    if params[:w] # just for show better view
      render partial: "new"
    end


  end

  def create
    params[:image][:user_id]=current_user.id
    @image=Image.new(image_params)
    @image_from_url = params[:image][:image_url].match(/(?i)\.(jpg|png|gif)\z/)
    @image.image_file = get_image_from_url(params[:image][:image_url]) if @image_from_url
    if @image.save
      render :show
    else render :new
    end

  end

  def show
    @image=Image.find(params[:id])

  end

  def hall_of_fame

  end


  def image_params
    params.require(:image).permit(:title, :description, :category_id, :user_id, :image_file, :image_url)
  end

  def get_image_from_url(url)
    begin
      open(url)
    rescue
      nil
    end
  end

  def assign_categories(owner)
    category_rows = owner.images.select("distinct category_id")
    category_ids = category_rows.collect { |a| a[:category_id] }
    @categories = Category.find(category_ids)
    return @categories
  end


end
