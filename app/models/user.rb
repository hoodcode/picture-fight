class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates_uniqueness_of :username
  has_many :achievements
  has_many :images
  has_many :votes
  has_many :comments

  attr_accessor :current_password
  def fights
    Fight.where("player_1_id = #{self.id} OR player_2_id = #{self.id}")
  end

  def to_param
    username
  end
end
