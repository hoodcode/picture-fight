class Image < ActiveRecord::Base
  has_attached_file :image_file, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment :image_file, :presence => true,
                       :content_type => { :content_type => "image/jpeg" },
                       :size => { :in => 0..5000.kilobytes}

  validates :title, length:{minimum: 5, maximum: 30}, :uniqueness => true


  belongs_to :user
  belongs_to :category



  def fight
    Fight.where("player_1_image_id = ? OR player_2_image_id = ?", self.id, self.id).first
  end


  attr_accessor :image_url


  def file_exist?
    FileTest.exist?(self.image_url)
  end

end
