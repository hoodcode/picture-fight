class Vote < ActiveRecord::Base
  validate :added_vote?
  validate :member_of_fight?
  validate :two_players_presence?

  belongs_to :user
  belongs_to :fight, :counter_cache => true
  belongs_to :for_player, :class_name=>"User"
end

 def added_vote?
   found=Vote.where(fight_id: self.fight_id, user_id: self.user_id)
   ok = found.empty?
     errors.add(:base, "You have already voted in this fight .") if !ok
 end

 def member_of_fight?
   fight=Fight.find(self.fight_id)
   if fight.player_1_id == self.user_id || fight.player_2_id == self.user_id
     errors.add(:base, "You are the member of fight, you can't vote.")
   end
 end

def two_players_presence?
  fight=Fight.find(self.fight_id)
  unless fight.player_1 && fight.player_2
    errors.add(:base, "You can vote when fight has 2 players !")
  end

end
