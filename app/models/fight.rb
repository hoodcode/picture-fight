class Fight < ActiveRecord::Base
  validate :has_uniq_images_among_fights?
  belongs_to :category
  belongs_to :player_1, class_name: "User"
  belongs_to :player_2, class_name: "User"
  belongs_to :player_1_image, class_name: "Image"
  belongs_to :player_2_image, class_name: "Image"
  has_many :comments
  has_many :votes
  belongs_to :winner, class_name: "User"

  scope :active_fights, -> { where("datetime_end > ?", DateTime.current).where("player_1_id is NOT NULL AND player_2_id is NOT NULL")}
  scope :closed_fights, -> { where("datetime_end <=?", DateTime.current)}



  def has_uniq_images_among_fights?
    found = Fight.where(:winner_id => nil).where("player_1_image_id IN (?, ?) OR player_2_image_id IN (?, ?)", player_1_image_id,player_2_image_id,player_1_image_id, player_2_image_id)
    ok = found.empty? || (found.size == 1 && found.first == self)
    errors.add(:base, "this picture already takes part in a fight.") if !ok
  end


end
