require 'spec_helper'

describe Category do

  before(:each) do
    @category = Category.new
  end

  it "can be created" do
    expect { @category.save }.to change { Category.count }.by 1
  end

  it "has many images" do
    @category.images.build
    @category.images.should_not be_nil
  end

  it "has many fights" do
    @category.fights.build
    @category.fights.should_not be_nil
  end


end
