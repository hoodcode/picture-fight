require 'spec_helper'

describe "Image" do

  before(:each) do
    @image = Image.new
  end

  it "can be created" do
    expect { @image.save }.to change { Image.count }.by 1
  end

  it "belongs to user" do
    @user=User.create
    @image.user_id=@user.id
    @image.user_id.should_not be_nil
  end

  it "belongs to category" do
    @category=Category.create
    @image.user_id=@category.id
    @image.user_id.should_not be_nil
  end

  it "belongs to fight" do
    @fight=Fight.create
    @image.user_id=@fight.id
    @image.user_id.should_not be_nil
  end

end