require 'spec_helper'

describe "Fight" do

  before(:each) do
    @fight = Fight.new

  end

  it "can be created" do
    expect { @fight.save }.to change { Fight.count }.by 1
  end

  it "has association with image of player one" do
    @image = Image.create()
    @fight.player_1_image_id=@image.id
    @fight.player_1_image_id.should_not be_nil
  end

  it "has association with image of player two" do
    @image2 = Image.create()
    @fight.player_2_image_id=@image2.id
    @fight.player_2_image_id.should_not be_nil
  end

  it "has many votes" do
    @fight.votes.build
    @fight.votes.should_not be_nil

  end

  it "has many comments" do
    @fight.comments.build
    @fight.comments.should_not be_nil
  end

  it "belongs to category" do
    @category = Category.create()
    @fight.category_id=@category.id
    @fight.category_id.should_not be_nil
  end

end