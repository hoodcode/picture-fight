class RemoveDateEndFromFight < ActiveRecord::Migration
  def change
    remove_column :fights, :date_end, :date
  end
end
