class AddVotesCount < ActiveRecord::Migration
  def change
    add_column :fights, :votes_count, :integer, :default => 0

    Fight.reset_column_information
    Fight.find_each do |p|
      Fight.update_counters p.id, :votes_count => p.votes.length
    end
  end
end