class CreateFights < ActiveRecord::Migration
  def change
    create_table :fights do |t|
      t.references :category, index: true
      t.references :player_1, index: true
      t.references :player_2, index: true
      t.references :player_1_image, index: true
      t.references :player_2_image, index: true
      t.date :date_end
      t.string :winner

      t.timestamps
    end
  end
end
