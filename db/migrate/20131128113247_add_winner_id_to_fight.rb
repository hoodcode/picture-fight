class AddWinnerIdToFight < ActiveRecord::Migration
  def change
    add_reference :fights, :winner, index: true
  end
end
