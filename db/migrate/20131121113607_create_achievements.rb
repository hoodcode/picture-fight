class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.references :achievement_db, index: true

      t.timestamps
    end
  end
end
