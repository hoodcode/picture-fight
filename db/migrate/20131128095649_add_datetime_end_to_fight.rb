class AddDatetimeEndToFight < ActiveRecord::Migration
  def change
    add_column :fights, :datetime_end, :datetime
  end
end
