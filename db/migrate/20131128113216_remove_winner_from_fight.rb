class RemoveWinnerFromFight < ActiveRecord::Migration
  def change
    remove_column :fights, :winner, :string
  end
end
