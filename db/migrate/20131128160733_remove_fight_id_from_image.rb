class RemoveFightIdFromImage < ActiveRecord::Migration
  def change
    remove_reference :images, :fight_id, index: true
  end
end
