class AddForPlayerToVote < ActiveRecord::Migration
  def change
    add_reference :votes, :for_player, index: true
  end
end
