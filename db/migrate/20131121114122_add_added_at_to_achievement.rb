class AddAddedAtToAchievement < ActiveRecord::Migration
  def change
    add_column :achievements, :added_at, :date
  end
end
