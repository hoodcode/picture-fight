class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :title
      t.string :description
      t.references :user, index: true
      t.references :category, index: true
      t.references :fight, index: true

      t.timestamps
    end
  end
end
