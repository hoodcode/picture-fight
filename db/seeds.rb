# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(name:"What_is_more_fun")
Category.create(name:"What_is_more_beautiful")
Category.create(name:"What_is_more_awful")
Category.create(name:"What_is_stronger")
Category.create(name:"What_is_faster")
Category.create(name:"What_is_more_stupid")
Category.create(name:"What_is_more_creative")
puts "success"
