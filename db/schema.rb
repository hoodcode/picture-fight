# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131129115800) do

  create_table "achievements", force: true do |t|
    t.integer  "achievement_db_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "added_at"
  end

  add_index "achievements", ["achievement_db_id"], name: "index_achievements_on_achievement_db_id"

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "fight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["fight_id"], name: "index_comments_on_fight_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "fights", force: true do |t|
    t.integer  "category_id"
    t.integer  "player_1_id"
    t.integer  "player_2_id"
    t.integer  "player_1_image_id"
    t.integer  "player_2_image_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "datetime_end"
    t.integer  "winner_id"
    t.integer  "votes_count",       default: 0
  end

  add_index "fights", ["category_id"], name: "index_fights_on_category_id"
  add_index "fights", ["player_1_id"], name: "index_fights_on_player_1_id"
  add_index "fights", ["player_1_image_id"], name: "index_fights_on_player_1_image_id"
  add_index "fights", ["player_2_id"], name: "index_fights_on_player_2_id"
  add_index "fights", ["player_2_image_id"], name: "index_fights_on_player_2_image_id"
  add_index "fights", ["winner_id"], name: "index_fights_on_winner_id"

  create_table "images", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "fight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_file_name"
    t.string   "image_file_content_type"
    t.integer  "image_file_file_size"
    t.datetime "image_file_updated_at"
  end

  add_index "images", ["category_id"], name: "index_images_on_category_id"
  add_index "images", ["fight_id"], name: "index_images_on_fight_id"
  add_index "images", ["user_id"], name: "index_images_on_user_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "username"
    t.string   "firstname"
    t.string   "surname"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "fight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "for_player_id"
  end

  add_index "votes", ["fight_id"], name: "index_votes_on_fight_id"
  add_index "votes", ["for_player_id"], name: "index_votes_on_for_player_id"
  add_index "votes", ["user_id"], name: "index_votes_on_user_id"

end
