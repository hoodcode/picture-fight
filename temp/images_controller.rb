def assign_categories(owner)
  category_rows = owner.images.select("distinct category_id")
  category_ids = category_rows.collect{ |a| a[:category_id]}
  @categories = Category.find(category_ids)
  return @categories
end

def search_by_category(category_name, owner=0)
  category = Category.where(name: category_name).first
  if owner!=0
    @images = owner.images.where(category_id: category.id)
  else
    @images = Image.all.where(category_id: category.id)
  end
  return @images
end